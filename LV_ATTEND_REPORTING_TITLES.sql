USE [impresario]
GO

    ALTER VIEW [dbo].[LV_ATTEND_REPORTING_TITLES] AS 
    SELECT [inv_no], 
           [description] 
    FROM [dbo].[T_INVENTORY] 
    WHERE [type] = 'T' 
      AND [inv_no] NOT IN (1148, 14375, 40674, 14376, 40679, 1395, 1398, 17825, 53191, 7179, 1126, 5541)

/*  Excluded Titles
    17825 = 3-D Theater         53191 = Cash Drop               7179  = CityPASS                14376 = Current Science & Technology
    40679 = d'Arbeloff Suite    1395  = Drop-In Activities      1398  = Live Presentations      1126  = Membership
    1148  = School Lunch        14375 = Science Live! Stage     5541  = Vouchers                40674 = Washburn Pavilion  */
GO


    GRANT SELECT ON [dbo].[LV_ATTEND_REPORTING_TITLES] TO [ImpUsers], [tessitura_app]
GO

    SELECT * FROM [dbo].[LV_ATTEND_REPORTING_TITLES]

