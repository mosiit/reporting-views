USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_SHOW_AND_GO]'))
    DROP VIEW [dbo].[LV_RPT_SHOW_AND_GO]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/*  PULLS SHOW AND GO INFORMATION -- cus.cust_type = 19 or 20 / CUST_TYPE 19 = N-Scan Show and Go / CUST_TYPE 20 = N-Scan Collected Pass 
                                                                                                    (20 removed from view by request of Phil)
    1/25/2020: Tweaked the code (Added the CTE) to make it run faster.  The CTE pulls Exhibit Show and Go Perfs  */

CREATE VIEW [dbo].[LV_RPT_SHOW_AND_GO] AS
WITH [CTE_SHOW_AND_GO_PERFS] ([perf_no], [perf_dt], [scan_title], [scan_production_name], [scan_production_name_long],
                              [scan_production_season_name], [scan_season_no], [scan_season])
AS (SELECT prf.[perf_no],
           prf.[perf_dt],
           ttl.[description],
           prt.[description],
           lng.[Value],
           sst.[description],
           sea.[season],
           ssn.[description] 
    FROM [dbo].[T_PERF] (NOLOCK) AS prf 
         INNER JOIN [dbo].[T_PROD_SEASON] (NOLOCK) AS sea ON sea.[prod_season_no] = prf.[prod_season_no]
         INNER JOIN [dbo].[T_INVENTORY] (NOLOCK) AS sst ON sst.[inv_no] = sea.[prod_season_no]
         INNER JOIN [dbo].[TR_SEASON] (NOLOCK) AS ssn ON ssn.[id] = sea.[season]
         INNER JOIN [dbo].[T_PRODUCTION] (NOLOCK) AS pro ON pro.[prod_no] = sea.[prod_no]
         INNER JOIN [dbo].[T_INVENTORY] (NOLOCK) AS prt ON prt.[inv_no] = pro.[prod_no]
         INNER JOIN [dbo].[TX_INV_CONTENT] (NOLOCK) AS lng ON lng.[inv_no] = pro.[prod_no] AND lng.[content_type] = 5
         INNER JOIN [dbo].[T_INVENTORY] (NOLOCK) AS ttl ON ttl.[inv_no] = pro.[title_no]
    WHERE ttl.[inv_no] = 7314)
SELECT  cus.[customer_no] as [show_and_go_no],
        cus.[cust_type] as [customer_type],
        cus.[lname] AS [show_and_go_name],
        att.[update_dt] as [scan_dt],
        prf.[perf_dt] as [performance_dt],
        prf.[scan_title] AS [scan_title],
        prf.[scan_production_name] AS [scan_production_name],
        prf.[scan_production_name_long] AS [scan_production_name_long],
        prf.[scan_production_season_name],
        prf.[scan_season],
        CONVERT(char(10),att.[update_dt],111) as [scan_date],
        CONVERT(char(8),att.[update_dt],108) as [scan_time],
        ISNULL(att.[device_name],'') as [device_name],
        1 as [scan_admission],
        [ticket_msg] as [ticket_msg]
FROM [dbo].[T_NSCAN_EVENT_CONTROL] as att (NOLOCK)
     INNER JOIN [dbo].[T_CUSTOMER] as cus (NOLOCK) ON cus.[customer_no] = att.[customer_no]
     LEFT OUTER JOIN [CTE_SHOW_AND_GO_PERFS] AS prf ON prf.[perf_no] = att.[perf_no]
WHERE (cus.cust_type = 19 and att.[ticket_msg] in ('OK','OK - Ticket already recorded'))
GO

GRANT SELECT ON [dbo].[LV_RPT_SHOW_AND_GO] TO [impusers], [tessitura_app]
GO

--FOR TESTING
SELECT * FROM [dbo].[LV_RPT_SHOW_AND_GO] WHERE scan_date = '2020/01/21' ORDER BY show_and_go_name



