USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_COUPON_REDEMPTION]'))
    DROP VIEW [dbo].[LV_RPT_COUPON_REDEMPTION]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_RPT_COUPON_REDEMPTION] AS
SELECT  sli.[order_no]
       ,sli.[perf_no]
       ,sli.[zone_no]
       ,prf.[title_name]
       ,prf.[performance_dt]
       ,prf.[performance_date]
       ,prf.[performance_time]
       ,prf.[production_name]
       ,prf.[production_name_long]
       ,sli.[price_type]
       ,pty.[description] as 'price_type_name'
       ,IsNull(sli.[comp_code], 0) as 'comp_code'
       ,IsNull(cmp.[description],pty.[description]) as 'comp_code_name'
       ,1 as 'sale_admission'
       ,isnull(att.admission_adult, 0) + IsNull(att.admission_child, 0) + IsNull(att.admission_other, 0) as 'scan_admission'
       ,sli.[sli_status]
       ,sls.[description] as 'sli_status_name'
FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
     INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] and prf.[performance_zone] = sli.[zone_no]
     INNER JOIN [dbo].[TR_PRICE_TYPE] as pty (NOLOCK) ON pty.[id] = sli.[price_type]
     INNER JOIN [dbo].[TR_SLI_STATUS] as sls (NOLOCK) ON sls.[id] = sli.[sli_status]
     LEFT OUTER JOIN [dbo].[TR_COMP_CODE] as cmp (NOLOCK) ON cmp.[id] = sli.[comp_code]
     LEFT OUTER JOIN [dbo].[T_ATTENDANCE] as att (NOLOCK) ON att.[ticket_no] = sli.[ticket_no]
WHERE sli.[price_type] in (SELECT [id] FROM [dbo].[TR_PRICE_TYPE] WHERE [price_type_category] in (18,19))
  and sli.[sli_status] in (3, 12)
GO

GRANT SELECT ON [dbo].[LV_RPT_COUPON_REDEMPTION] TO Impusers
GO


SELECT * FROM LV_RPT_COUPON_REDEMPTION WHERE performance_date = '2016/07/19'
