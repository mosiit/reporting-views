USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_CASHOUT_MEMBERSHIPS]'))
    DROP VIEW [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] AS
SELECT   sli.[sli_no]
        ,sli.[li_seq_no]
        ,sli.[due_amt]
        ,sli.[paid_amt]
        ,sli.[price_type] as [price_type_no]
        ,typ.[description] as [price_type_name]
        ,CASE WHEN ISNULL(sli.[rule_id],0) = 1218 THEN 'Y' ELSE 'N' END AS [is_one_step_recovery]
        ,CASE WHEN ISNULL(sli.[recipient_no],0) > 0 AND ISNULL(sli.[recipient_no],0) <> ISNULL(ord.[customer_no],0) THEN 'Y' ELSE 'N' END AS [is_gift_membership]
        ,ISNULL(sli.[comp_code],0) as [comp_code_no]
        ,sli.[sli_status] as [sli_status_no]
        ,sta.[description] AS [sli_status_name]
        ,sta.[status_code] AS [sli_status_code]
        ,sli.[seat_no]
        ,sli.[ticket_no]
        ,ISNULL(sli.[cancel_ind],'N') AS [cancel_ind]
        ,sli.[created_by] as [membership_operator]
        ,sli.[create_loc] AS [membership_create_location]
        ,usr.[fname] AS [membership_operator_first_name]
        ,usr.[lname] AS [membership_operator_last_name]
        ,usr.[location] AS [membership_operator_location]
        ,sli.[create_dt] AS [membership_dt]
        ,sli.[create_loc] AS [membership_machine]
        ,CONVERT(CHAR(10),sli.[create_dt],111) AS [membership_date]
        ,CONVERT(CHAR(8),sli.[create_dt],108) AS [membershipcoupon_time]
        ,sli.[perf_no]
        ,sli.[zone_no]
        ,sli.[batch_no]
        ,sli.[order_no]
        ,prf.[performance_code]
        ,prf.[title_name]
        ,prf.[production_name]
        ,prf.[performance_zone_text] as [membership_type] 
        ,ord.[customer_no]
        ,ISNULL(sli.[recipient_no],0) AS [recipient_no]
        ,ord.[order_dt]
        ,[dbo].[LFS_GET_ONE_STEP_IND] (sli.[sli_no])as [add_one_step]
        ,[dbo].[LFS_GET_ONE_STEP_COUNT] (sli.[sli_no])AS [one_step_count]
FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
     LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ  (NOLOCK) ON typ.[id] = sli.[price_type]
     LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] as sta  (NOLOCK) ON sta.[id] = sli.[sli_status]
     LEFT OUTER JOIN [dbo].[T_METUSER] as usr (NOLOCK) ON usr.[userid] = sli.[last_updated_by]
     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] and prf.[performance_zone] = sli.[zone_no]
     LEFT OUTER JOIN [dbo].[T_ORDER] as ord (NOLOCK) ON ord.order_no = sli.[order_no]
WHERE prf.[title_name] = 'Membership'
GO

GRANT SELECT ON [dbo].[LV_RPT_CASHOUT_MEMBERSHIPS] TO impusers
GO


SELECT * FROM LV_RPT_CASHOUT_MEMBERSHIPS WHERE [membership_date] between '2017/10/27' and '2017/11/11' AND is_gift_membership = 'N'-- and membership_operator = 'kaltom00' --AND IS_one_step_recovery = 'Y' --AND one_step_count = 1 ORDER BY one_step_count
