USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_SHOW_AND_COLLECTED]'))
    DROP VIEW [dbo].[LV_RPT_SHOW_AND_COLLECTED]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

/* PULLS SHOW AND GO COLLECTED PASS INFORMATION -- cus.cust_type = 20 (N-Scan Collected Pass)
        1/25/2020: Added code to deal with new City Pass Scans
                   Also, tweaked the code (Added the CTE) to make it run faster.  The CTE pulls Exhibit Halls and Exhibit Show and Go Perfs  */

CREATE VIEW [dbo].[LV_RPT_SHOW_AND_COLLECTED] AS
WITH [CTE_SHOW_AND_GO_PERFS] ([perf_no], [perf_dt], [scan_title], [scan_production_name], [scan_production_name_long],
                              [scan_production_season_name], [scan_season_no], [scan_season])
AS (SELECT prf.[perf_no],
           prf.[perf_dt],
           ttl.[description],
           prt.[description],
           lng.[Value],
           sst.[description],
           sea.[season],
           ssn.[description] 
    FROM [dbo].[T_PERF] (NOLOCK) AS prf 
         INNER JOIN [dbo].[T_PROD_SEASON] (NOLOCK) AS sea ON sea.[prod_season_no] = prf.[prod_season_no]
         INNER JOIN [dbo].[T_INVENTORY] (NOLOCK) AS sst ON sst.[inv_no] = sea.[prod_season_no]
         INNER JOIN [dbo].[TR_SEASON] (NOLOCK) AS ssn ON ssn.[id] = sea.[season]
         INNER JOIN [dbo].[T_PRODUCTION] (NOLOCK) AS pro ON pro.[prod_no] = sea.[prod_no]
         INNER JOIN [dbo].[T_INVENTORY] (NOLOCK) AS prt ON prt.[inv_no] = pro.[prod_no]
         INNER JOIN [dbo].[TX_INV_CONTENT] (NOLOCK) AS lng ON lng.[inv_no] = pro.[prod_no] AND lng.[content_type] = 5
         INNER JOIN [dbo].[T_INVENTORY] (NOLOCK) AS ttl ON ttl.[inv_no] = pro.[title_no]
    WHERE (ttl.[inv_no] = 27 AND prt.[inv_no] = 32)
       OR ttl.[inv_no] = 7314)
SELECT cus.[customer_no] as [show_and_collected_no],
       cus.[cust_type] as [customer_type],
       CASE WHEN att.[ticket_msg] = 'OK CityPass' AND LEN(LTRIM(RTRIM(att.[scan_str]))) = 14 THEN 'CityPass Booklet Scans'
            WHEN att.[ticket_msg] = 'OK CityPass' AND LEN(LTRIM(RTRIM(att.[scan_str]))) <> 14 THEN 'CityPass Mobile Scans'
            ELSE cus.[lname] END AS [show_and_collected_name],
       att.[update_dt] as [scan_dt],
       prf.[perf_dt] as [performance_dt],
       CASE WHEN att.[ticket_msg] = 'OK CityPASS' THEN 'Exhibit Show and Go'
             ELSE prf.[scan_title] END AS [scan_title],
       CASE WHEN att.[ticket_msg] = 'OK CityPASS' THEN 'Exhibit Show and Go'
            ELSE prf.[scan_production_name] END AS [scan_production_name],
       CASE WHEN att.[ticket_msg] = 'OK CityPASS' THEN 'Exhibit Hall Show and Go Admission'
            ELSE prf.[scan_production_name_long] END AS [scan_production_name_long],
       prf.[scan_production_season_name],
       prf.[scan_season],
       convert(char(10),att.[update_dt],111) as [scan_date],
       convert(char(8),att.[update_dt],108) as [scan_time],
       IsNull(att.[device_name],'') as [device_name],
       1 as [scan_admission],
       att.[ticket_msg] as [ticket_msg],
       att.[scan_str]
FROM [dbo].[T_NSCAN_EVENT_CONTROL] as att (NOLOCK)
     INNER JOIN [dbo].[T_CUSTOMER] as cus (NOLOCK) ON cus.[customer_no] = att.[customer_no]
     INNER JOIN [CTE_SHOW_AND_GO_PERFS] AS prf ON prf.[perf_no] = att.[perf_no]
     --INNER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = att.[perf_no]
WHERE (cus.cust_type = 20 and att.[ticket_msg] in ('OK','OK - Ticket already recorded'))
   OR att.[ticket_msg] = 'OK CityPASS'
GO

GRANT SELECT ON [dbo].[LV_RPT_SHOW_AND_COLLECTED] TO impusers
GO

----FOR TESTING
--SELECT * FROM [dbo].[LV_RPT_SHOW_AND_COLLECTED] WHERE scan_date = '2020/02/22' ORDER BY show_and_collected_name, scan_dt


