USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_CASHOUT_ADD_ONS]'))
    DROP VIEW [dbo].[LV_RPT_CASHOUT_ADD_ONS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_RPT_CASHOUT_ADD_ONS] AS
SELECT   sli.[sli_no]
        ,sli.[li_seq_no]
        ,sli.[due_amt]
        ,sli.[paid_amt]
        ,sli.[price_type] as 'price_type_no'
        ,typ.[description] as 'price_type_name'
        ,sli.[comp_code] as 'comp_code_no'
        ,sli.[sli_status] as 'sli_status_no'
        ,sta.[description] as 'sli_status_name'
        ,sta.[status_code] as 'sli_status_code'
        ,sli.[seat_no]
        ,sli.[ticket_no]
        ,sli.[cancel_ind]
        ,sli.[created_by] as 'product_operator'
        ,usr.[fname] as 'product_operator_first_name'
        ,usr.[lname] as 'product_operator_last_name'
        ,usr.[location] as 'product_operator_location'
        ,sli.[create_dt] as 'product_dt'
        ,convert(char(10),sli.[create_dt],111) as 'product_date'
        ,convert(char(8),sli.[create_dt],108) as 'product_time'
        ,sli.[perf_no]
        ,sli.[zone_no]
        ,sli.[batch_no]
        ,sli.[order_no]
        ,prf.[performance_code]
        ,prf.[title_name]
        ,prf.[production_name]
        ,CASE WHEN aty.[description] = 'Add One Step' THEN 'Y' ELSE 'N' END as 'add_one_step'
        ,CASE WHEN aty.[description] = 'Add One Step' THEN 1 ELSE 0 END as 'one_step_count'
FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
     LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ  (NOLOCK) ON typ.[id] = sli.[price_type]
     LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] as sta  (NOLOCK) ON sta.[id] = sli.[sli_status]
     LEFT OUTER JOIN [dbo].[T_METUSER] as usr (NOLOCK) ON usr.[userid] = sli.[last_updated_by]
     LEFT OUTER JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] as prf (NOLOCK) ON prf.[performance_no] = sli.[perf_no] and prf.[performance_zone] = sli.[zone_no]
     LEFT OUTER JOIN [dbo].[T_ORDER] as ord (NOLOCK) ON ord.order_no = sli.[order_no]
     LEFT OUTER JOIN [dbo].[T_CUST_ACTIVITY] as csi (NOLOCK) ON csi.[customer_no] = ord.[customer_no]
     LEFT OUTER JOIN [dbo].[TR_CUST_ACTIVITY_TYPE] as aty (NOLOCK) ON aty.[id] = csi.[activity_type]
WHERE left(typ.[description],6) in ('Add-on','Add on')
GO

GRANT SELECT ON [dbo].[LV_RPT_CASHOUT_ADD_ONS] to impusers
GO

