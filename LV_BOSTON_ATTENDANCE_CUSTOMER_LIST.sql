
    ALTER VIEW [dbo].[LV_BOSTON_ATTENDANCE_CUSTOMER_LIST] AS
    WITH [CTE_ADDRESS_LIST]
    AS (SELECT ROW_NUMBER() OVER(PARTITION BY [customer_no] ORDER BY CASE WHEN [address_type] = 3 THEN 'A' ELSE 'B' END, 
                                                                     [address_type], [primary_ind] DESC,  [last_update_dt] DESC) AS [address_row_no],
               [address_no],
               [customer_no],
               [address_type],
               [primary_ind],
               [city],
               [state],
               LEFT([postal_code],5) AS [postal_code]
        FROM [dbo].[T_ADDRESS]
        WHERE [address_type] not in (1, 20)
          AND [inactive] = 'N'
          AND [customer_no] NOT IN (SELECT [customer_no] FROM [dbo].[TX_CONST_CUST] (NOLOCK) WHERE [constituency] = 61)
          AND LEFT(postal_code,5) IN ('02101', '02108', '02109', '02110', '02111', '02112', '02113', '02114', '02115', '02116', '02117', '02118', '02119', '02120',
                                      '02121', '02122', '02123', '02124', '02125', '02126', '02127', '02128', '02129', '02130', '02131', '02132', '02133', '02133',
                                      '02134', '02135', '02136', '02137', '02163', '02196', '02199', '02201', '02203', '02204', '02205', '02206', '02210', '02211',
                                      '02212', '02215', '02217', '02222', '02228', '02241', '02266', '02283', '02284', '02293', '02297', '02298')
          AND ([address_type] = 3 OR ([address_type] <> 3 AND [primary_ind] = 'Y')))
    SELECT pa.[expanded_customer_no] AS [customer_no],
           cte.[address_no],
           adr.[address_type],
           typ.[description] AS [address_type_name],
           ISNULL(adr.[street1], '') AS [street1],
           ISNULL(adr.[street2], '') AS [street2],
           ISNULL(adr.[street3], '') AS [street3],
           ISNULL(adr.[city], '') AS [city],
           ISNULL(adr.[state], '') AS [state],
           LEFT(ISNULL(adr.[postal_code],''),5) AS [postal_code],
           ISNULL(adr.[postal_code],'') AS [zip_plus4],
           ISNULL(adr.[last_updated_by], '') AS [last_updated_by]
    FROM [T_CUSTOMER] (NOLOCK) cus
         INNER JOIN [CTE_ADDRESS_LIST] AS cte ON cte.[customer_no] = cus.[customer_no] AND [cte].[address_row_no] = 1
         INNER JOIN [dbo].[T_ADDRESS] AS adr ON adr.[address_no] = cte.[address_no]
         INNER JOIN [dbo].[TR_ADDRESS_TYPE] AS typ ON typ.[id] = adr.[address_type]
         INNER JOIN V_CUSTOMER_WITH_PRIMARY_AFFILIATES (NOLOCK) pa ON cus.customer_no = pa.customer_no
    WHERE cus.inactive = 1
      AND cte.[address_row_no] = 1
      AND (cus.[cust_type] IN (3,5,7,9,10,11,12,13,14,15,20,21,22)
           OR (cus.[cust_type] = 1 AND cus.[customer_no] NOT IN (SELECT c2.[customer_no] 
                                                                 FROM [dbo].[T_CUSTOMER] (NOLOCK) c2 
                                                                      INNER JOIN [dbo].[T_AFFILIATION] (NOLOCK) a ON c2.[customer_no] = a.[individual_customer_no]
                                                                 WHERE c2.[cust_type] = 1
                                                                   AND a.primary_ind = 'Y' 
                                                                   AND a.[affiliation_type_id] IN (10002,10003))))
  GO

  GRANT SELECT ON [dbo].[LV_BOSTON_ATTENDANCE_CUSTOMER_LIST] TO [ImpUsers], [tessitura_app]
  GO


  SELECT * FROM [dbo].[LV_BOSTON_ATTENDANCE_CUSTOMER_LIST]