USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_CASHOUT_COUPONS]'))
    DROP VIEW [dbo].[LV_RPT_CASHOUT_COUPONS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_RPT_CASHOUT_COUPONS] AS
SELECT   sli.[sli_no]
        ,sli.[li_seq_no]
        ,sli.[due_amt]
        ,sli.[paid_amt]
        ,sli.[price_type] as 'price_type_no'
        ,  typ.[description] as 'price_type_name'
        ,CASE WHEN sli.[comp_code] is null THEN 0 ELSE sli.[comp_code] END as 'comp_code_no'
        ,CASE WHEN typ.[id] IN (364,365,366) THEN 'EH LIBRARY 50%'
              WHEN cmp.[description] is null THEN typ.[description] 
              ELSE cmp.[description] END as 'comp_code_name'
        ,CASE WHEN typ.[id] IN (364,365,366) THEN 'LP_L50%'
              WHEN cmp.[short_desc] is NULL THEN 'LP_' + right(typ.description,4) 
              ELSE cmp.[short_desc] END as 'comp_code_name_short'
        ,CASE WHEN cmp.[inactive] is null THEN 'N' ELSE cmp.[inactive] END as 'comp_code_inactive'
        ,sli.[sli_status] as 'sli_status_no'
        ,sta.[description] as 'sli_status_name'
        ,sta.[status_code] as 'sli_status_code'
        ,sli.[seat_no]
        ,sli.[ticket_no]
        ,sli.[cancel_ind]
        ,CASE WHEN ISNULL(loc.[description],'') IN ('Membership','Science Central') THEN sli.[last_updated_by] ELSE sli.[created_by] END as 'coupon_operator'
        ,CASE WHEN ISNULL(loc.[description],'') IN ('Membership','Science Central') THEN usr.[fname] ELSE uso.[fname] END as 'coupon_operator_first_name'
        ,CASE WHEN ISNULL(loc.[description],'') IN ('Membership','Science Central') THEN usr.[lname] ELSE uso.[lname] END AS 'coupon_operator_last_name'
        ,CASE WHEN ISNULL(loc.[description],'') IN ('Membership','Science Central') THEN usr.[location] ELSE uso.[location] END as 'coupon_operator_location'
        ,sli.[last_update_dt] as 'coupon_dt'
        ,convert(char(10),sli.[last_update_dt],111) as 'coupon_date'
        ,convert(char(8),sli.[last_update_dt],108) as 'coupon_time'
        ,sli.[created_by] as 'coupon_operator_original'
        ,sli.[create_dt] as 'coupon_dt_original'
        ,convert(char(10),sli.[create_dt],111) as 'coupon_date_original'
        ,convert(char(8),sli.[create_dt],108) as 'coupon_time_original'
        ,sli.[create_loc] AS 'coupon_machine_original'
        ,ISNULL(loc.[description],'') AS 'coupon_location_original'
        ,sli.[perf_no]
        ,sli.[zone_no]
        ,sli.[batch_no]
        ,sli.[order_no]
FROM [dbo].[T_SUB_LINEITEM] as sli (NOLOCK)
     LEFT OUTER JOIN [dbo].[TR_PRICE_TYPE] as typ  (NOLOCK) ON typ.[id] = sli.[price_type]
     LEFT OUTER JOIN [dbo].[TR_COMP_CODE] as cmp  (NOLOCK) ON cmp.[id] = sli.[comp_code]
     LEFT OUTER JOIN [dbo].[TR_SLI_STATUS] as sta  (NOLOCK) ON sta.[id] = sli.[sli_status]
     LEFT OUTER JOIN [dbo].[T_METUSER] as usr (NOLOCK) ON usr.[userid] = sli.[last_updated_by]
     LEFT OUTER JOIN [dbo].[T_METUSER] as uso (NOLOCK) ON uso.[userid] = sli.[created_by]
     LEFT OUTER JOIN [dbo].[TX_MACHINE_LOCATION] AS olo (NOLOCK) ON olo.[machine_name] = sli.[create_loc]
     LEFT OUTER JOIN [dbo].[TR_LOCATION] AS loc (NOLOCK) ON loc.[id] = olo.[location]
WHERE (sli.comp_code is not null 
       OR typ.[description] like 'Exhibit Halls Library%'
       OR typ.[description] LIKE 'EH Library 50%') 
  AND sli.[sli_status] in (3, 12)
GO

GRANT SELECT ON [dbo].[LV_RPT_CASHOUT_COUPONS] TO impusers
GO
    
SELECT * FROM [dbo].[LV_RPT_CASHOUT_COUPONS] WHERE CAST([coupon_dt] AS DATE) = '1-2-2020' AND price_type_name LIKE '%library%'




