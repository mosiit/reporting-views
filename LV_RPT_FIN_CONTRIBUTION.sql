USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_FIN_CONTRIBUTION_1]'))
    DROP VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_1]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_FIN_CONTRIBUTION_2]'))
    DROP VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_2]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_1] AS
      SELECT  c.[ref_no]
             ,c.[customer_no]
             ,cu.[cust_type] as 'customer_type_no'
             ,typ.[description] as 'customer_type_name'
             ,IsNull(cu.[fname], '') as 'customer_first_name'
             ,IsNull(cu.[mname], '') as 'customer_middle_name'
             ,IsNull(cu.[lname], '') as 'customer_last_name'
             ,IsNull(cu.[sort_name], '') as 'customer_sort_name'
             ,c.[cont_type] as 'contribution_type'
             ,c.[cont_dt] as 'contribution_dt'
             ,IsNull(convert(char(10),c.[cont_dt],111), '') as 'contribution_date'
             ,IsNull(convert(char(8),c.[cont_dt],108), '') as 'contribution_time'
             ,c.[cont_amt] as 'contribution_amount'
             ,f.[description] as 'fund_description'
             ,f.[nonrestricted_income_gl_no]
             ,substring (f.[nonrestricted_income_gl_no], 15, 4) as 'source'
             ,0 as 'creditee_no'
             ,0 as 'creditee_type_no'
             ,'' as 'creditee_type_name'
             ,'' as 'creditee_first_name'
             ,'' as 'creditee_middle_name'
             ,'' as 'creditee_last_name'
             ,'' as 'creditee_sort_name'
      FROM [dbo].[T_CONTRIBUTION] as c (NOLOCK)
           INNER JOIN [dbo].[LTR_FUND_DETAIL] as fd (NOLOCK) ON c.[fund_no] = fd.[fund_no]
           INNER JOIN [dbo].[T_FUND] as f (NOLOCK) ON c.[fund_no] = f.[fund_no]
           INNER JOIN [dbo].[T_CAMPAIGN] as cm (NOLOCK) ON c.[campaign_no] = cm.[campaign_no]
           INNER JOIN [dbo].[T_CUSTOMER] as cu (NOLOCK) ON c.[customer_no] = cu.[customer_no]
           INNER JOIN [dbo].[TR_CUST_TYPE] as typ (NOLOCK) ON typ.[id] = cu.[cust_type]
      WHERE c.[cont_type] = 'G' 
        and c.[ref_no] not in (SELECT [ref_no] FROM [dbo].[T_CONTRIBUTION] WHERE [custom_1] = 'Primary Soft Credit')
        and c.[customer_no] <> 2653093 
        and fd.[acct_grp_no] not in (3, 5, 9) 
        and cm.[category] not in (1, 2, 3, 4, 24, 41, 42, 43, 44)  
        and cu.[cust_type] <> 5
GO

GRANT SELECT ON [dbo].[LV_RPT_FIN_CONTRIBUTION_1] TO impusers
GO

CREATE VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_2] AS
      SELECT  c.[ref_no]
             ,c.[customer_no]
             ,cu1.[cust_type] as 'customer_type_no'
             ,typ.[description] as 'customer_type_name'
             ,IsNull(cu1.[fname], '') as 'customer_first_name'
             ,IsNull(cu1.[mname], '') as 'customer_middle_name'
             ,IsNull(cu1.[lname], '') as 'customer_last_name'
             ,IsNull(cu1.[sort_name], '') as 'customer_sort_name'
             ,c.[cont_type] as 'contribution_type'
             ,c.[cont_dt] as 'contribution_dt'
             ,IsNull(convert(char(10),c.[cont_dt],111), '') as 'contribution_date'
             ,IsNull(convert(char(8),c.[cont_dt],108), '') as 'contribution_time'
             ,c.[cont_amt] as 'contribution_amount'
             ,f.[description] as 'fund_description'
             ,f.[nonrestricted_income_gl_no]
             ,substring (f.[nonrestricted_income_gl_no], 15, 4) as 'source'
             ,t.[creditee_no]
             ,cu2.[cust_type] as 'creditee_type_no'
             ,ctp.[description] as 'creditee_type_name'
             ,IsNull(cu2.[fname], '') as 'creditee_first_name'
             ,IsNull(cu2.[mname], '') as 'creditee_middle_name'
             ,IsNull(cu2.[lname], '') as 'creditee_last_name'
             ,IsNull(cu2.[sort_name], '') as 'creditee_sort_name'
      FROM [dbo].[T_CONTRIBUTION] as c (NOLOCK)
           INNER JOIN [dbo].[LTR_FUND_DETAIL] as fd (NOLOCK) ON c.[fund_no] = fd.[fund_no]
           INNER JOIN [dbo].[T_FUND] as f (NOLOCK) ON c.[fund_no] = f.[fund_no]
           INNER JOIN [dbo].[T_CAMPAIGN] as cm (NOLOCK) ON c.[campaign_no] = cm.[campaign_no]
           INNER JOIN [dbo].[T_CUSTOMER] AS cu1 ON c.[customer_no] = cu1.[customer_no]
           INNER JOIN [dbo].[TR_CUST_TYPE] as typ (NOLOCK) ON typ.[id] = cu1.[cust_type]
           INNER JOIN [dbo].[T_CREDITEE] AS t (NOLOCK) ON c.[ref_no] = t.[ref_no]
           INNER JOIN [dbo].[T_CUSTOMER] AS cu2 ON t.[creditee_no] = cu2.[customer_no]
           INNER JOIN [dbo].[TR_CUST_TYPE] as ctp (NOLOCK) ON ctp.[id] = cu2.[cust_type]
     WHERE c.[cont_type] = 'G' 
       and c.[custom_1] = 'Primary Soft Credit' 
       and c.[customer_no] <> 2653093 
       and fd.[acct_grp_no] not in (3,5,9) 
       and t.[creditee_type] in (5,12,15) 
       and cm.[category] not in (1, 2, 3, 4, 24, 41, 42, 43, 44) 
       and cu1.[cust_type] <> 5
GO

GRANT SELECT ON [dbo].[LV_RPT_FIN_CONTRIBUTION_2] TO impusers
GO

--SELECT * FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_1] WHERE contribution_date between '2016/06/01' and '2016/06/30'
--SELECT * FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_2] WHERE contribution_date between '2016/06/01' and '2016/06/30'


