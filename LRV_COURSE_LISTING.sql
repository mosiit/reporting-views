USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LRV_COURSE_LISTING]'))
    DROP VIEW [dbo].[LRV_COURSE_LISTING]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LRV_COURSE_LISTING] AS
      SELECT    -4 AS 'performance_no'
              , NULL AS 'performance_dt'
              , NULL AS 'performance_date'
              , NULL AS 'performance_time'
              , NULL AS 'performance_type'
              , 'All Courses' AS 'production_name'
              , 'All Courses' AS 'production_name_long'
              , '_All Courses' AS 'performance_name'
UNION SELECT    -3 AS 'performance_no'
              , NULL AS 'performance_dt'
              , NULL AS 'performance_date'
              , NULL AS 'performance_time'
              , NULL AS 'performance_type'
              , 'Full Day Courses' AS 'production_name'
              , 'Full Day Courses' AS 'production_name_long'
              , '_Only Full Day Courses' AS 'performance_name'
UNION SELECT    -2 AS 'performance_no'
              , NULL AS 'performance_dt'
              , NULL AS 'performance_date'
              , NULL AS 'performance_time'
              , NULL AS 'performance_type'
              , 'AM Courses' AS 'production_name'
              , 'AM Courses' AS 'production_name_long'
              , '_Only AM Courses' AS 'performance_name'
UNION SELECT    -1 AS 'performance_no'
              , NULL AS 'performance_dt'
              , NULL AS 'performance_date'
              , NULL AS 'performance_time'
              , NULL AS 'performance_type'
              , 'PM Courses' AS 'production_name'
              , 'PM Courses' AS 'production_name_long'
              , '_Only PM Courses' AS 'performance_name'
UNION SELECT    [performance_no]
              , [performance_dt]
              , [performance_date]
              , [performance_time]
              , [performance_time_display] AS 'performance_type'
              , [production_name]
              , [production_name_long]
              , CASE WHEN [performance_time] < '12:00' THEN 'AM' ELSE 'PM' END + ' ' + [production_name] + ' (' + RIGHT(performance_date,5) + ')' AS 'performance_name'
FROM [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE]
      WHERE [performance_dt] BETWEEN CONVERT(DATETIME,'7-1-' + DATENAME(YEAR,GETDATE()) + ' 00:00:00') AND CONVERT(DATETIME,'8-31-' + DATENAME(YEAR,GETDATE()) + ' 23:59:59')
        and [title_name] = 'Summer Courses'
GO

GRANT SELECT ON [dbo].[LRV_COURSE_LISTING] TO ImpUsers
GO


SELECT performance_no, performance_name FROM LRV_COURSE_LISTING 
WHERE CONVERT(DATE,performance_dt) BETWEEN '7-10-2017' AND '7-10-2017' OR performance_no < 0
ORDER BY [performance_name]


--WHERE performance_dt BETWEEN '7-10-2017' AND '7-17-2017 23:59:59' OR performance_dt IS NULL
--ORDER BY Production_Name_display



--SELECT performance_no FROM [LRV_COURSE_LISTING] WHERE performance_dt BETWEEN '7-10-2017' AND '7-17-2017 23:59:59' AND performance_time < '12:00'



