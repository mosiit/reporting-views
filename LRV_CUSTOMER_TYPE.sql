USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LRV_CUSTOMER_TYPE]'))
    DROP VIEW [dbo].[LRV_CUSTOMER_TYPE]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LRV_CUSTOMER_TYPE] AS
      SELECT 0 AS 'id', '_all customer types' AS 'description'
UNION SELECT [id], [description] FROM dbo.TR_CUST_TYPE 
      WHERE inactive = 'N' AND description NOT LIKE 'N-Scan%' AND description NOT like '%Scholarship%'
GO

GRANT SELECT ON [dbo].[LRV_CUSTOMER_TYPE] TO ImpUsers
GO

SELECT * FROM [dbo].[LRV_CUSTOMER_TYPE] ORDER BY description

