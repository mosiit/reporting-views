USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_CASHOUT_PAYMENTS]'))
    DROP VIEW [dbo].[LV_RPT_CASHOUT_PAYMENTS]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_RPT_CASHOUT_PAYMENTS] AS
SELECT  pay.[payment_no] as 'payment_no'
       ,pay.[sequence_no] as 'sequence_no'
       ,pay.[transaction_no] as 'transaction_no'
       ,pay.[customer_no] as 'customer_no'
       ,pay.[pmt_dt] as 'payment_dt'
       ,convert(char(10),pay.[pmt_dt],111) as 'payment_date'
       ,convert(char(8),pay.[pmt_dt],108) as 'payment_time'
       ,trn.[create_loc] AS 'transaction_machine'
       ,pay.[pmt_amt] as 'payment_amount'
       ,pay.[tendered_amt] as 'tendered_amount'
       ,pay.[pmt_method] as 'payment_method_no'
       ,mth.[description] as 'payment_method_name'
       ,mth.[short_desc] as 'payment_method_name_short'
       ,mth.[pmt_type] as 'payment_type_no'
       ,typ.[description] as 'payment_type_name'
       ,ISNULL(pay.[account_no],'') as 'account_no'
       ,ISNULL(pay.[check_no], 0) as 'check_no'
       ,ISNULL(pay.[check_name],'') as 'check_name'
       ,pay.[card_expiry_dt] as 'card_expiry_dt'
       ,pay.[auth_ind] as 'auth_ind'
       ,ISNULL(pay.[auth_no],'') AS 'auth_no'
       ,pay.[created_by] as 'payment_operator'
       ,usr.[fname] as 'payment_operator_first_name'
       ,usr.[lname] as 'payment_operator_last_name'
       ,usr.[location] as 'payment_operator_location'
       ,pay.[batch_no] as 'batch_no'
       ,bat.[batch_type] as'batch_type_no'
       ,btp.[description] as 'batch_type_name'
       ,bat.[owner] as 'batch_owner'
       ,bat.[closed_by] as 'batch_closed_by'
       ,ISNULL(bat.[posted_by],'') as 'batch_posted by'
       ,bat.[posted_dt] as 'posted_dt'
       ,ISNULL(bat.[notes],'') as 'batch_notes'
       ,pay.[refund_dt] as 'refund_dt'
       ,ISNULL(pay.[ccref_no],'') as 'ccref_no'
       ,ISNULL(pay.[cc_issue_no],'') as 'cc_issue_no'
       ,pay.[cc_start_date] as 'cc_start_date'
       ,ISNULL(pay.[act_id],0) as 'act_id'
       ,ISNULL(pay.[notes],'') as 'payment_notes'
       ,CASE WHEN pay.[parent_payment_no] is null THEN 0 ELSE pay.[parent_payment_no] END as 'parent_payment_no'
FROM [dbo].[T_PAYMENT] as pay (NOLOCK)
     LEFT OUTER JOIN [dbo].[T_TRANSACTION] AS trn (NOLOCK) ON trn.[transaction_no] = pay.[transaction_no] AND trn.[sequence_no] = pay.[sequence_no]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_METHOD] as mth (NOLOCK) ON mth.[id] = pay.[pmt_method]
     LEFT OUTER JOIN [dbo].[TR_PAYMENT_TYPE] as typ (NOLOCK) ON typ.[id] = mth.[pmt_type]
     LEFT OUTER JOIN [dbo].[T_BATCH] as bat (NOLOCK) ON bat.[batch_no] = pay.[batch_no]
     LEFT OUTER JOIN [dbo].[TR_BATCH_TYPE] as btp (NOLOCK) ON btp.[id] = bat.[batch_type]
     LEFT OUTER JOIN [dbo].[T_METUSER] as usr (NOLOCK) ON usr.[userid] = pay.[created_by]
WHERE pay.[pmt_method] <> 152                  --152 = CityPASS Pre-Paid Voucher -- not really a payment, needs to be excluded
GO

GRANT SELECT ON [dbo].[LV_RPT_CASHOUT_PAYMENTS] to impusers
GO


--SELECT * FROM dbo.LV_RPT_CASHOUT_PAYMENTS WHERE payment_date = '2017/04/04'
----696596

----SELECT userid, fname, lname, location FROM dbo.T_METUSER
--SELECT * FROM dbo.T_PAYMENT WHERE CONVERT(DATE,pmt_dt) = '4-18-2017'