USE [impresario]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_1]'))
    DROP VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_1]
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_2]'))
    DROP VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_2]
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_1] AS
      SELECT  c.[ref_no]
             ,c.[customer_no]
             ,cu.[cust_type] as 'customer_type_no'
             ,typ.[description] as 'customer_type_name'
             ,IsNull(cu.[fname], '') as 'customer_first_name'
             ,IsNull(cu.[mname], '') as 'customer_middle_name'
             ,IsNull(cu.[lname], '') as 'customer_last_name'
             ,IsNull(cu.[sort_name], '') as 'customer_sort_name'
             ,c.[cont_type] as 'contribution_type'
             ,c.[cont_dt] as 'contribution_dt'
             ,IsNull(convert(char(10),c.[cont_dt],111), '') as 'contribution_date'
             ,IsNull(convert(char(8),c.[cont_dt],108), '') as 'contribution_time'
             ,c.[cont_amt] as 'contribution_amount'
             ,f.[description] as 'fund_description'
             ,f.[nonrestricted_income_gl_no]
             ,substring (f.[nonrestricted_income_gl_no], 15, 4) as 'source'
             ,0 as 'creditee_no'
             ,0 as 'creditee_type_no'
             ,'' as 'creditee_type_name'
             ,'' as 'creditee_first_name'
             ,'' as 'creditee_middle_name'
             ,'' as 'creditee_last_name'
             ,'' as 'creditee_sort_name'
        FROM [dbo].[T_CONTRIBUTION] AS c (NOLOCK)
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS fd (NOLOCK) ON c.[fund_no] = fd.[fund_no]
             INNER JOIN [dbo].[T_FUND] as f (NOLOCK) ON c.[fund_no] = f.[fund_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cm (NOLOCK) ON c.[campaign_no] = cm.[campaign_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS cu (NOLOCK) ON c.[customer_no] = cu.[customer_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] as typ (NOLOCK) ON typ.[id] = cu.[cust_type]
       WHERE c.cont_type = 'G'
         and c.customer_no <> 2653093
         and fd.acct_grp_no not in (3,5,9) 
         and cm.category NOT IN (1, 2, 3, 4, 24,41, 42, 43, 44) 
         and cu.cust_type = 5
GO

GRANT SELECT ON [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_1] TO impusers
GO

CREATE VIEW [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_2] AS
      SELECT  c.[ref_no]
             ,c.[customer_no]
             ,cu.[cust_type] as 'customer_type_no'
             ,typ.[description] as 'customer_type_name'
             ,IsNull(cu.[fname], '') as 'customer_first_name'
             ,IsNull(cu.[mname], '') as 'customer_middle_name'
             ,IsNull(cu.[lname], '') as 'customer_last_name'
             ,IsNull(cu.[sort_name], '') as 'customer_sort_name'
             ,tt.[description] as 'contribution_type'
             ,p.[pmt_dt] as 'contribution_dt'
             ,IsNull(convert(char(10),p.[pmt_dt],111), '') as 'contribution_date'
             ,IsNull(convert(char(8),p.[pmt_dt],108), '') as 'contribution_time'
             ,p.[pmt_amt] as 'contribution_amount'
             ,f.[description] as 'fund_description'
             ,f.[nonrestricted_income_gl_no]
             ,substring (f.[nonrestricted_income_gl_no], 15, 4) as 'source'
             ,0 as 'creditee_no'
             ,0 as 'creditee_type_no'
             ,'' as 'creditee_type_name'
             ,'' as 'creditee_first_name'
             ,'' as 'creditee_middle_name'
             ,'' as 'creditee_last_name'
             ,'' as 'creditee_sort_name'
        FROM [dbo].[T_CONTRIBUTION] AS c (NOLOCK)
             INNER JOIN [dbo].[LTR_FUND_DETAIL] AS fd (NOLOCK) ON c.[fund_no] = fd.[fund_no]
             INNER JOIN [dbo].[T_FUND] as f (NOLOCK) ON c.[fund_no] = f.[fund_no]
             INNER JOIN [dbo].[T_CAMPAIGN] AS cm (NOLOCK) ON c.[campaign_no] = cm.[campaign_no]
             INNER JOIN [dbo].[T_CUSTOMER] AS cu (NOLOCK) ON c.[customer_no] = cu.[customer_no]
             INNER JOIN [dbo].[T_TRANSACTION] AS tr (NOLOCK) ON c.[ref_no] = tr.[ref_no]
             INNER JOIN [dbo].[TR_TRANSACTION_TYPE] AS tt (NOLOCK) ON tr.[trn_type] = tt.[id]
             INNER JOIN [dbo].[T_PAYMENT] AS p (NOLOCK) ON tr.[sequence_no] = p.[sequence_no]
             LEFT OUTER JOIN [dbo].[TR_CUST_TYPE] as typ (NOLOCK) ON typ.[id] = cu.[cust_type]
       WHERE c.[cont_type] = 'P'
         and c.[customer_no] <> 2653093
         and fd.[acct_grp_no] not in (3, 5, 9)
         and cm.[category] NOT IN (1, 2, 3, 4, 24, 41, 42, 43, 44)
         and tr.trn_type in (3, 6)
         and cu.cust_type = 5 
GO

GRANT SELECT ON [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_2] TO impusers
GO


--SELECT * FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_1]  WHERE contribution_date between '2015/07/01' and '2016/06/30' ORDER BY contribution_dt DESC
--SELECT * FROM [dbo].[LV_RPT_FIN_CONTRIBUTION_GOVERNMENT_2]  WHERE contribution_date between '2015/07/01' and '2016/06/30' ORDER BY contribution_dt DESC

